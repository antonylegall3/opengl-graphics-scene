README for a C++ program written to utilize OpenGL graphics to render an underwater animation complete with the ability to rotate, zoom, speed up, slow down, pause the animation and change the cell shading.

Repository File Structure:
    - imageloader.cpp - Code to load images into the program as textures
    - imageloader.h - Header file for imageloader.cpp
    - main.cpp - Main file containing all source code to model,
    - render and animate my scene
    - Makefile - file to compile the entire program
    - .bmp Files - image files used to generate textures

Compilation:
	- Navigate to the main directory
    
        $ make

Running:
	- Navigate to the main directory
    
        $ ./main
