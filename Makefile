CC = g++
CFLAGS = -Wall
PROG = main

SRCS = main.cpp imageloader.cpp imageloader.h


all: $(PROG)

$(PROG):	$(SRCS)
	$(CC) $(CFLAGS) -o $(PROG) $(SRCS) $(LIBS) -lglut -lGLU -lGL

clean:
	rm -f $(PROG)