#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include "imageloader.h"
#include <GL/freeglut.h>

#define WINDOW_WIDTH	800
#define WINDOW_HEIGHT	800
using namespace std;

//Shade Mode
static GLenum shadeMode = GL_SMOOTH;

//Text on screen variable
PFNGLWINDOWPOS2IPROC glWindowPos2i;

//Texture Array
GLuint textureArray[4];

//Materials
GLfloat bankMaterial[] = {0.0, 0.0, 0.0};
GLfloat whiteSpecularMaterial[] = {1.0, 1.0, 1.0};
GLfloat purpleDiffuseMaterial[] = {0.6, 0.2, 1.0};
GLfloat yellowEmissiveMaterial[] = {1.0, 1.0, 0.0};
GLfloat mShininess[] = {128};

//Animation values
int animater = 0;
float zoomVal = -12.0f;
float fishTranslator = -0.5;
float boxTranslator = -0.5;
float bubbleTranslator = 0.25;
float rotator = 1.0;
//Specify the angles
float _angle = 0.0f;
float cameraAngle = 0.0f;
float xRotateVal = 0.0f;
float yRotateVal = 0.0f;
int timerVar = 50;
//Forward declarations
void animate(int value);
void rotateX(int value);
void zoom(int value);
GLuint loadTexture(Image* image);
void rotateY(int value);

//Function to handle key presses
void handleKeypress(unsigned char key, int x, int y) {
	switch (key) {
		//Zoom In (Z)
		case 'Z':
			zoom(key);
			break;
		//Zoom Out (z)
		case 'z':
			zoom(key);
			break;
		//Rotate X (x/X)
		case 'X':
		case 'x':
			rotateX(key);
			//glutTimerFunc(25, rotate, 0);
			break;
		//Rotate Y (y/Y)
		case 'y':
		case 'Y':
			rotateY(key);
			break;
		//Animate (a/A)
		case 'a':
		case 'A':
			animater = 1;
			animate(key);
			break;
		//Pause animation (t/T)
		case 't':
		case 'T':
			animater = 0;
			break;
		//Resume animation (c/C)
		case 'c':
		case 'C':
			animater = 1;
			glutTimerFunc(25, animate, 0);
			break;
		//Increase speed (f/F)
		case 'f':
		case 'F':
			if(timerVar > 0)
			{
				timerVar -= 10;
			}
			break;
		//Decrease speed (s/S)
		case 's':
		case 'S':
			timerVar += 10;
			break;
		case 'p':
		
			shadeMode = GL_FLAT;
			break;
		case 'P':
			shadeMode = GL_SMOOTH;
			break;
		//Exit (esc)
		case 27:
			exit(0);
	}
}

//Function called once at the beginning of execution to initialize variables
void initRendering() {

	//Load textures
	Image* image = loadBMP("box.bmp");
	textureArray[0] = loadTexture(image);
	image = loadBMP("fish.bmp");
	textureArray[1] = loadTexture(image);
	image = loadBMP("sand.bmp");
	textureArray[2] = loadTexture(image);
	image = loadBMP("rock.bmp");
	textureArray[3] = loadTexture(image);

	glEnable(GL_DEPTH_TEST);
	//Make openGL scale all the normal vectors so they have a magnitude of 1
	glEnable(GL_NORMALIZE);
	glEnable(GL_COLOR_MATERIAL);

	//Enable transparency for objects
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//Specify a background color
	glClearColor(0.5f, 0.6f, 1.0f, 1);
}

//Function to handle the window being resized
void handleResize(int w, int h) {
	//Makes it so we have 3D perspective (only used when resizing window)
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glViewport(0, 0, w, h);
	gluPerspective(45.0, (float)w / (float)h, 1.0, 400.0);
}

//Function to create fog
void fog()
{
	//Enable Fog
	glEnable(GL_FOG);
	//Specify color of fog
	GLfloat fogColor[] = {0.5f, 0.5f, 0.5f, 1.0};
	//Tell openGL what color fog we are using
	glFogfv(GL_FOG_COLOR, fogColor);
	glFogi(GL_FOG_MODE, GL_LINEAR);
	glFogf(GL_FOG_START, 8.0f);
	glFogf(GL_FOG_END, 10.0f);
}

//Function to create light sources
void lighting()
{
	//Turn on Lighting
	glEnable(GL_LIGHTING);

	//Ambient light shines everywhere in the scene by the same amount
	GLfloat ambientLight[] = {0.6f, 0.6f, 0.6f, 1.0f};
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientLight);
	
	//Enable light 0
	glEnable(GL_LIGHT0);
	//Add a positioned light
	GLfloat lightColor[] = {0.7f, 0.7f, 0.7f, 1.0f};
	//pass it the light, diffuse flag is to tell it we setting the color
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor);
	GLfloat lightPos[] = {7.0f, 7.0f, 8.0, 1.0f};
	//tell openGL the position of our light
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos);


	//Enable light 1
	glEnable(GL_LIGHT1);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor);
	GLfloat lightPos1[] = {0.0f, 0.0f, 10.0, 1.0f};
	//tell openGL the position of our light
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos1);
}

//Function to draw the floor
void drawFloor()
{
	//Enable the textures
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureArray[2]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
	glPushMatrix();
	//Move the box 20 units in the negative Z direction
	glTranslatef(0.0f, -1.0f, -5.0f);

	//Scale the box in all planes
	glScalef(3.0f, 0.05f, 3.0f);
	//Draw a box
	glBegin(GL_QUADS);
	
	//Top face
	glNormal3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-7.0f / 2, 7.0f / 2, -7.0f / 2);
	glVertex3f(-7.0f / 2, 7.0f / 2, 7.0f / 2);
	glVertex3f(7.0f / 2, 7.0f / 2, 7.0f / 2);
	glVertex3f(7.0f / 2, 7.0f / 2, -7.0f / 2);
	
	//Bottom face
	glNormal3f(0.0, -1.0f, 0.0f);
	glVertex3f(-7.0f / 2, -7.0f / 2, -7.0f / 2);
	glNormal3f(0.0, -1.0f, 0.0f);
	glVertex3f(7.0f / 2, -7.0f / 2, -7.0f / 2);
	glVertex3f(7.0f / 2, -7.0f / 2, 7.0f / 2);
	glVertex3f(-7.0f / 2, -7.0f / 2, 7.0f / 2);
	
	//Left face
	glNormal3f(-1.0, 0.0f, 0.0f);
	glVertex3f(-7.0f / 2, -7.0f / 2, -7.0f / 2);
	glVertex3f(-7.0f / 2, -7.0f / 2, 7.0f / 2);
	glVertex3f(-7.0f / 2, 7.0f / 2, 7.0f / 2);
	glVertex3f(-7.0f / 2, 7.0f / 2, -7.0f / 2);
	
	//Right face
	glNormal3f(1.0, 0.0f, 0.0f);
	glVertex3f(7.0f / 2, -7.0f / 2, -7.0f / 2);
	glVertex3f(7.0f / 2, 7.0f / 2, -7.0f / 2);
	glVertex3f(7.0f / 2, 7.0f / 2, 7.0f / 2);
	glVertex3f(7.0f / 2, -7.0f / 2, 7.0f / 2);
	
	//Front face
	glNormal3f(0.0, 0.0f, 1.0f);
	glVertex3f(-7.0f / 2, -7.0f / 2, 7.0f / 2);
	glVertex3f(7.0f / 2, -7.0f / 2, 7.0f / 2);
	glVertex3f(7.0f / 2, 7.0f / 2, 7.0f / 2);
	glVertex3f(-7.0f / 2, 7.0f / 2, 7.0f / 2);
	
	//Back face
	glNormal3f(0.0, 0.0f, -1.0f);
	glVertex3f(-7.0f / 2, -7.0f / 2, -7.0f / 2);
	glVertex3f(-7.0f / 2, 7.0f / 2, -7.0f / 2);
	glVertex3f(7.0f / 2, 7.0f / 2, -7.0f / 2);
	glVertex3f(7.0f / 2, -7.0f / 2, -7.0f / 2);
	
	glEnd();
	glPopMatrix();

    glDisable(GL_TEXTURE_2D);
}

//Function to draw rocks on ocean floor
void drawRocks()
{
	//Enable the texture
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureArray[3]);
    glEnable(GL_TEXTURE_GEN_S);
    glEnable(GL_TEXTURE_GEN_T);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);


	//Rock 1
	glPushMatrix();
	//Set Material
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, purpleDiffuseMaterial);
	glTranslatef(1.5f, -0.6f, -5.0f);
	glRotatef(45, 1.0f, 1.0f, 0.0f);
	glColor3f(0.7f, 0.7f, 0.7f);
	glutSolidSphere(0.5f, 3, 3);
	glPopMatrix();

	//Rock2
	glPushMatrix();
	glTranslatef(1.0f, -0.75f, -8.0f);
	glutSolidSphere(0.5f, 4, 4);
	glPopMatrix();

	//Composite Rock3
	glPushMatrix();
	glTranslatef(-1.0f, -0.6f, -4.0f);
	glRotatef(45, 1.0f, 1.0f, 0.0f);
	glColor3f(0.7f, 0.7f, 0.7f);
	glutSolidSphere(0.5f, 3, 3);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-1.0f, -0.75f, -3.7f);
	glutSolidSphere(0.5f, 4, 4);
	glPopMatrix();

	//Disable the texture functions
	glDisable(GL_TEXTURE_GEN_S);
    glDisable(GL_TEXTURE_GEN_T);
    glDisable(GL_TEXTURE_2D);
}

//Function to draw the box
void drawBox()
{
	//Enable the texture
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureArray[0]);
    glEnable(GL_TEXTURE_GEN_S); //enable texture coordinate generation
    glEnable(GL_TEXTURE_GEN_T);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glPushMatrix();
	glTranslatef(-0.25f, boxTranslator, -10.0f);

	//Scale the box in all planes
	glScalef(0.05f, 0.05f, 0.05f);
	//Draw a box
	glBegin(GL_QUADS);
	
	//Top face
	glNormal3f(0.0, 1.0f, 0.0f);
	glVertex3f(-7.0f / 2, 7.0f / 2, -7.0f / 2);
	glVertex3f(-7.0f / 2, 7.0f / 2, 7.0f / 2);
	glVertex3f(7.0f / 2, 7.0f / 2, 7.0f / 2);
	glVertex3f(7.0f / 2, 7.0f / 2, -7.0f / 2);
	
	//Bottom face
	glNormal3f(0.0, -1.0f, 0.0f);
	glVertex3f(-7.0f / 2, -7.0f / 2, -7.0f / 2);
	glNormal3f(0.0, -1.0f, 0.0f);
	glVertex3f(7.0f / 2, -7.0f / 2, -7.0f / 2);
	glVertex3f(7.0f / 2, -7.0f / 2, 7.0f / 2);
	glVertex3f(-7.0f / 2, -7.0f / 2, 7.0f / 2);
	
	//Left face
	glNormal3f(-1.0, 0.0f, 0.0f);
	glVertex3f(-7.0f / 2, -7.0f / 2, -7.0f / 2);
	glVertex3f(-7.0f / 2, -7.0f / 2, 7.0f / 2);
	glVertex3f(-7.0f / 2, 7.0f / 2, 7.0f / 2);
	glVertex3f(-7.0f / 2, 7.0f / 2, -7.0f / 2);
	
	//Right face
	glNormal3f(1.0, 0.0f, 0.0f);
	glVertex3f(7.0f / 2, -7.0f / 2, -7.0f / 2);
	glVertex3f(7.0f / 2, 7.0f / 2, -7.0f / 2);
	glVertex3f(7.0f / 2, 7.0f / 2, 7.0f / 2);
	glVertex3f(7.0f / 2, -7.0f / 2, 7.0f / 2);
	
	//Front face
	glNormal3f(0.0, 0.0f, 1.0f);
	glVertex3f(-7.0f / 2, -7.0f / 2, 7.0f / 2);
	glVertex3f(7.0f / 2, -7.0f / 2, 7.0f / 2);
	glVertex3f(7.0f / 2, 7.0f / 2, 7.0f / 2);
	glVertex3f(-7.0f / 2, 7.0f / 2, 7.0f / 2);
	
	//Back face
	glNormal3f(0.0, 0.0f, -1.0f);
	glVertex3f(-7.0f / 2, -7.0f / 2, -7.0f / 2);
	glVertex3f(-7.0f / 2, 7.0f / 2, -7.0f / 2);
	glVertex3f(7.0f / 2, 7.0f / 2, -7.0f / 2);
	glVertex3f(7.0f / 2, -7.0f / 2, -7.0f / 2);
	
	glEnd();
	glPopMatrix();

	//Disable the texture
	glDisable(GL_TEXTURE_GEN_S);
    glDisable(GL_TEXTURE_GEN_T);
    glDisable(GL_TEXTURE_2D);
}

//Function to draw the fish
void drawFish()
{
	//Set Material
	glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, mShininess);
	//Enable the texture
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureArray[1]);
    glEnable(GL_TEXTURE_GEN_S); //enable texture coordinate generation
    glEnable(GL_TEXTURE_GEN_T);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	//Draw the Fish Sphere
	glPushMatrix();
	glTranslatef(fishTranslator, 0.0f, -5.0f);
	glScalef(1.0f, 0.5f, 0.7f);
	glutSolidSphere(0.5f, 25, 25);
	glPopMatrix();

	//Draw the traingle fin
	glPushMatrix();
	glTranslatef(fishTranslator, 0.02f, -5.0f);
	glRotatef(10, 1.0, 0.0, 0.0);
	glScalef(0.5f, 0.5f, 0.5f);
	glBegin(GL_TRIANGLES);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 0.5f, 0.0f);
	glVertex3f(1.0f, -0.5f, 0.0f);
	glEnd();
	glPopMatrix();

	//Disable the texture
	glDisable(GL_TEXTURE_GEN_S);
    glDisable(GL_TEXTURE_GEN_T);
    glDisable(GL_TEXTURE_2D);
}

//Function to draw the buoy
void drawBuoy()
{
	//Apply white specular material
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, whiteSpecularMaterial);
	//Draw the sphere buoy
	glPushMatrix();
	glTranslatef(1.0f, 1.5f, -6.0f);
	glRotatef(90, 1.0f, 0.0f, 0.0f);
	glutSolidCylinder(0.03, 2.5, 10, 10);
	glPopMatrix();

	//Apply Shininess
	glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, mShininess);

	//Draw the cylinder chain
	glPushMatrix();
	glTranslatef(1.0f, 1.2f, -6.0f);
	glColor3f(0.7f, 0.7f, 0.7f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glutSolidSphere(0.5f, 9, 9);
	glPopMatrix();
}

//Function to draw bubbles
void drawBubbles()
{
	//Draw Bubble 1
	glPushMatrix();
	glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
	glTranslatef(0.85f, bubbleTranslator, -7.8f);
	glutSolidSphere(0.2f, 30, 30);
	glPopMatrix();

	//Draw bubble 2
	glPushMatrix();
	glTranslatef(0.85f, bubbleTranslator + 0.5, -7.8f);
	glutSolidSphere(0.15f, 30, 30);
	glPopMatrix();

	//Draw bubble 3
	glPushMatrix();
	glTranslatef(0.85f, bubbleTranslator + 0.8, -7.8f);
	glutSolidSphere(0.1f, 30, 30);
	glPopMatrix();
}

//Draw the help text on the screen
void drawHelpTextLine(char* string, int lineNum)
{
    int i;
    int j = strlen(string);
    glColor3f(1.0, 1.0, 1.0);
    //Specify the position of the txt on the screen
    glWindowPos2i(10, WINDOW_HEIGHT - (lineNum + 1) * 15);

    for(i = 0; i < j; i++) 
    {
    	//Write the text to the screen
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_10, string[i]);
    }
}

//Draw the text ont he screen
void drawHelpText(void)
{
    char text[] = "zoom in <Z> and out <z>\n"
                    "rotate about x-axis <x> <X> or y-axis <y> <Y>\n"
                    "run animation <a> <A>\n"
                    "pause animation <t> <T>\n"
                    "resume animation <C> <c>\n"
                    "increase animation speed <f> <F>\n"
                    "decrease animation speed <s> <S>\n"
                    "flat shaded animation <p>\n"
                    "smooth shaded animation <P>\n"
                    "quit <esc>";
    char* ptr = NULL;
    int lineNum = 0;
    ptr = strtok(text, "\n");
    while (ptr != NULL)
    {
        drawHelpTextLine(ptr, lineNum);
        ptr = strtok(NULL, "\n");
        lineNum++;
    }
}

//Function to load textures onto the screen
GLuint loadTexture(Image* image) {
	GLuint textureId;
	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_2D, textureId); 
	//Map the image to the texture
	glTexImage2D(GL_TEXTURE_2D,         
				 0,                       
				 GL_RGB,                    
				 image->width, image->height, 
				 0,                            
				 GL_RGB, 
				 GL_UNSIGNED_BYTE,		            
				 image->pixels);       
	return textureId;
}
//Main function to draw the scene
void drawScene() 
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//Set the camera positioning
	gluLookAt(0.0f, 0.0f, zoomVal,
				0.0f, 0.0f, -2.0f,
				0.0f, 1.0f, 0.0f);
	glRotatef(xRotateVal, 1.0, 0.0, 0.0);
	glRotatef(yRotateVal, 0.0, 1.0, 0.0);

	//Draw the help text
	glWindowPos2i =  (PFNGLWINDOWPOS2IPROC) glutGetProcAddress("glWindowPos2i");
	drawHelpText();
	glPushMatrix();
	//Draw the fog
	fog();

	//Set up the lights
	lighting();

	//Set the shade mode
	glShadeModel(shadeMode);

	//Draw the Floor
	glPushMatrix();
	drawFloor();
	glPopMatrix();

	//Draw some Rocks
	drawRocks();

	//Draw the Box
	drawBox();

	//Draw Fish
	drawFish();

	//Draw the Buoy
	drawBuoy();

	//Draw some Bubbles
	drawBubbles();

	glPopMatrix();

	//Swap draw and static buffers
	glutSwapBuffers();
}

//Called every 25 milliseconds to update the angle x degrees
void animate(int value) {
	if(animater == 1)
	{
		boxTranslator += 0.004f;
		fishTranslator -= 0.02f;
		bubbleTranslator += 0.02f;
		rotator += 1.0f;
		//Tell glut to call update again in timerVarms, 0 is the value parameter
		glutTimerFunc(timerVar, animate, 0);
	}
}

//Function to rotate about the X plane
void rotateX(int value)
{
	xRotateVal -= 0.1f;
	glutTimerFunc(25, rotateX, 0);
}

//Function to rotate about the Y plane
void rotateY(int value)
{
	yRotateVal += 0.1f;
	glutTimerFunc(25, rotateY, 0);
}

//When called we want to scale the scene either in or out by 0.1
void zoom(int value)
{
	//Zoom in
	if(value == 122)
	{
		zoomVal = zoomVal - zoomVal * 0.01;
	}
	//Zoom out
	else
	{
		zoomVal = zoomVal + zoomVal * 0.01;
	}
}

void idle(void)
{
	//Tell openGL the scene has changed and to redraw it
	glutPostRedisplay();
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	//Specify the size of the window
	glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
	
	//Create the window
	glutCreateWindow("CG200 Assignment: 'Under the Sea'");
	//Specify settings, whats enabled etc.
	initRendering();
	
	//Call the drawscene func
	glutDisplayFunc(drawScene);
	//Set event handlers
	glutReshapeFunc(handleResize);
	glutKeyboardFunc(handleKeypress);
	//Function called to tell openGL to redraw the scene
	glutIdleFunc(idle);
	//Loop over main
	glutMainLoop();
	return 0;
}